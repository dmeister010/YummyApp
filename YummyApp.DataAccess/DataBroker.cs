﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YummyApp.DataAccess
{
    public class DataBroker
    {
        private YummyEntityModelContainer context = new YummyEntityModelContainer();

        public void Seed()
        {
            var customers = new List<Customer>
            {
                new Customer { FirstName="Mya", LastName="Pham",
                               Address1 ="1546 Le St", City="Chantilly", State="VA",
                               Zipcode="20150", Username="user", Password="pass",
                               CreditCardNumber ="4567"
                             },
                new Customer { FirstName="Alex", LastName="Kuni"},
                new Customer { FirstName="Helga", LastName="Mono"},
            };

            customers.ForEach(s => context.Customers.Add(s));
            context.SaveChanges();

            var sizes = new List<Size>
            {
                new Size {Name="Small", Price=6.99},
                new Size {Name="Medium", Price=8.99},
                new Size {Name="Large", Price=10.99},
                new Size {Name="Extra Large", Price=12.99},
            };

            sizes.ForEach(s => context.Sizes.Add(s));
            context.SaveChanges();

            //var toppings = new List<Topping>
            //{
            //    new Topping {Name="Pepperoni", Price=0.99},
            //    new Topping {Name="Beef", Price=0.99},
            //    new Topping {Name="Ham", Price=0.99},
            //    new Topping {Name="Sausage", Price=0.99},
            //    new Topping {Name="Extra Cheese", Price=0.99},
            //    new Topping {Name="Olive", Price=0.99},
            //    new Topping {Name="Tomato", Price=0.99},
            //    new Topping {Name="Pineapple", Price=0.99},
            //};

            //toppings.ForEach(s => context.Toppings.Add(s));
            //context.SaveChanges();

            //Order order = new Order { CustomerID = 1, SizeID = 1 };
            //context.Orders.Add(order);
            //SelectedTopping oneTopping = new SelectedTopping { OrderID = 1, ToppingID = 1 };
            //context.SelectedToppings.Add(oneTopping);

            context.SaveChanges();


            var sides = new List<Side>
            {
                new Side {Name="Garlic Breads", Price=4.99},
                new Side {Name="Chicken Wings", Price=6.99},
                new Side {Name="Cheese Sticks", Price=12.99},
            };

            sides.ForEach(s => context.Sides.Add(s));
            context.SaveChanges();

            //SelectedSide oneSide = new SelectedSide { OrderID = 1, SideID = 1 };
            //context.SelectedSides.Add(oneSide);
            context.SaveChanges();
        }
    }
}